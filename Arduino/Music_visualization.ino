  
//Constants
const int pResistor = A1; // Photoresistor at Arduino analog pin A0
const int ledPin=9;       // Led pin at Arduino pin 9

//Variables
int value;          // Store value from photoresistor (0-1023)

void setup(){
 //pinMode(ledPin, OUTPUT);  // Set lepPin - 9 pin as an output
 pinMode(pResistor, INPUT);// Set pResistor - A0 pin as an input (optional)
 Serial.begin(9600);
   Serial.println("DHTxx test!");
}

//Constantly reads data from the light sensor 
void loop(){
  value = analogRead(pResistor);
  float t =value;
  Serial.println((byte)t);
  delay(1500); //Small delay
}

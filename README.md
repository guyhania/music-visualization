# Music visualizer

Reactive interface that uses light sensor inputs to create soundsthrough synthesizer along with music visualization. 

## Created with:

*Arduino -  [Arduino - Home](https://www.arduino.cc/)
*Pure Data - [Pure Data — Pd Community Site](https://puredata.info/)
*Processing  - [Processing Site](https://processing.org/)


### Watch demonstration:  [Link](https://www.youtube.com/watch?v=z-o2BQIidUI&feature=youtu.be)
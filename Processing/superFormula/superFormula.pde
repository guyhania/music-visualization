/* oscP5 and netP5 allow data transfer between pure data and proccessing */
import oscP5.*;
import netP5.*;

float value;
float x;
float sin3;
float mainFr;

OscP5 oscP5;
NetAddress myRemoteLocation;
float v;


void setup() {
 size(1920,1080); 
 noFill();
 stroke(255);
 //stroke.setStroke(200);
 strokeWeight(2);
 oscP5 = new OscP5(this,12000);
 myRemoteLocation = new NetAddress("127.0.0.1",12000);

  
}


void oscEvent(OscMessage theOscMessage){
  if(theOscMessage.checkAddrPattern("/sinosc")==true)
  {
     value = theOscMessage.get(0).floatValue();
   println("This is VAL:  "+value);
  }
 if(theOscMessage.checkAddrPattern("/x")==true)
  {
     x = theOscMessage.get(0).floatValue();
    println("This is SEC:"+x);
  }
   if(theOscMessage.checkAddrPattern("/sinThird")==true)
  {
     sin3 = theOscMessage.get(0).floatValue();
    println("This is sin3:"+sin3);
  }
    if(theOscMessage.checkAddrPattern("/main")==true)
  {
     mainFr = theOscMessage.get(0).floatValue();
    println("This is MAin:"+mainFr);
  }
}

float t=0;
void draw(){
  background(0);
  translate(width/2,height/2);
  beginShape();
  
  for(float theta = 0; theta<=2 *PI ; theta+=0.0001){
     float rad= r(theta,
     mainFr, //a
      mainFr, //b
   sin3, //m
  2, //n1
     sin(t)*0.5+0.5, //n2
     cos(t)*0.5+0.5
     ); //n3
     float x = rad * cos(theta) * 50;
     float y = rad * sin(theta) * 50;
     vertex(x,y);
   
  }
  
  endShape();
 
  t += 0.6;
}



//formula for creating the visualization shapes

float r(float theta, float a,float b, float m,float n1, float n2,float n3){
  return pow(pow(abs(cos(m*theta/4.0)/a),n2)+ 
    pow(abs(sin(m*theta/4.0)/b),n3),-1/n1);
  
}
public float test(float x){
float c=x;
return c;
}
